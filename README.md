# idex-ticker-get-proxy

This is an application which will proxy the IDEX API on your own machine, but it will map GET requests to JSON POST requests so you can consume the API using software which only supports GET, such as Microsoft Excel.

## Installation

Clone this repo, then in the project directory use

```shell
npm install
```

## Usage

In the project directory, run

```shell
npm start
```

By default, this will host a server on http://localhost:8080 with the same routes as the IDEX API server, so you can load ticker data by making a GET request to http://localhost:8080/returnTicker or load the orderbook with http://localhost:8080/returnOrderBook etc.

## Author

Raymond Pulver IV
