'use strict';

const startServer = require('./proxy');

startServer();

const repl = require('repl');
const r = repl.start('> ');
Object.assign(r.context, {
  config: require('./config'),
  proxy: require('./proxy')
});
