'use strict';

const express = require('express');
const cors = require('cors')
const config = require('./config');
const moment = require('moment');
const request = require('request');
const { promisify } = require('bluebird');
const {
  bindKey,
  property
} = require('lodash');
const chalk = require('chalk');
const log = bindKey(console, 'log');
const error = bindKey(console, 'error');
const { flow } = require('lodash/fp');
const url = require('url');
const port = config.port || 8080;
const host = '0.0.0.0';


const info = (v) => log(chalk.cyan('[' + moment().format('MM-DD HH:mm:ss') + ']') + ' ' + String(v));

const app = express();
app.use(cors());
app.all('/:route', (req, res) => config.endpoints.includes(req.params.route) || config.endpoints.includes('all') ? (info('request to ' + chalk.yellow('/' + req.params.route) + ' from ' + chalk.magenta(req.connection.remoteAddress)), request({
  method: 'POST',
  url: config.host + '/' + req.params.route,
  json: req.query
}).pipe(res)) : (res.status(405), res.send('Method not allowed')))
const startServer = promisify(bindKey(app, 'listen'));

module.exports = () => startServer(port, host).then(() => info('server started at ' + url.format({ protocol: 'http', hostname: host, port }))).catch(flow(
  property('stack'),
  error
));
